/*
 * zeno_imu_sensor_fusion.cpp
 *
 * Created on: March 30, 2015
 * modified on: May 28, 2016
 * Author: Shehzad
 */
#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>
 
#include <iostream>
#include <time.h>
#include <boost/shared_ptr.hpp>

#include "zeno_i2c_interface/ADXL345.h"
#include "zeno_i2c_interface/L3G4200D.h"
#include <zeno_i2c_interface/zeno_i2c_interface.h>


#define Eigen2Support
#define EIGEN2_SUPPORT_STAGE10_FULL_EIGEN2_API
#include <Eigen/Geometry>

#define PI 3.14156

/****************************************
 * TYPEDEFS 
 ****************************************/

using namespace Eigen;


class IMUIntegration
{
  public:
      IMUIntegration(const ros::NodeHandle &nh, ZenoI2CInterface &zeno_i2c_interface):
        gyro_(zeno_i2c_interface),  accel_(zeno_i2c_interface), nh_(nh)
      {
        initialize();
      }
      /**
       * Initializes imu ROS interface
      */
      void initialize()
      {
        nh_.param<double>("cycle_rate", cycle_rate_, 10.0);

        nh_.param<double>("weight_gyro", weight_gyro_, 0.5);
        nh_.param<double>("gyro_calib_x", gyro_calib_x_, -1.3);
        nh_.param<double>("gyro_calib_y", gyro_calib_y_, 0.40);
        nh_.param<double>("gyro_calib_z", gyro_calib_z_, 0.669669);

        nh_.param<double>("weight_accelerometer", weight_accelerometer_, 0.5);
        nh_.param<double>("accelerometer_calib_x", accelerometer_calib_x_, -0.03795);
        nh_.param<double>("accelerometer_calib_y", accelerometer_calib_y_, -0.0521981);
        nh_.param<double>("accelerometer_calib_z", accelerometer_calib_z_, 0.0365782);

        imu_state_pub_ = nh_.advertise <std_msgs::Float64MultiArray> ("torso_state", 1);

        accelerometer_state_pub_ = nh_.advertise < std_msgs::Float64MultiArray> ("accelerometer_state", 1);

        init_imu();
      }

      bool init_imu()
      {
        accel_.init();
        accel_.setSoftwareOffset(accelerometer_calib_x_,
                                  accelerometer_calib_y_,
                                  accelerometer_calib_z_); //(-x_calib,-y_calib,1-z_calib)
        //accel.printCalibrationValues(1000);

        xi = yi = zi = 0;
        roll.resize(3);
        pitch.resize(3);
        yaw.resize(3);
        gyro_.init(gyro_calib_x_, gyro_calib_y_, gyro_calib_z_);
        //gyro.printCalibrationValues(1000);

        angleF_roll = 0.0;
        angleF_pitch = 0.0;
      }

      void start()
      {
        ROS_INFO("IMU interface is now running.");
        ROS_INFO("Press Cntrl+c to kill the node.");
        int i=0;
        pT = clock();
        /*
        * Declaring and initilizing loop frequency.
        */
        ros::Rate loop_rate(cycle_rate_);

        while(ros::ok()) { 
              sleepcp(100);
              execute_cycle();
              ros::spinOnce();
              loop_rate.sleep();
        }
        ROS_INFO("Killing node on exit");
      }
 
      void execute_cycle()
      {
        //ACCEL
        AccelRotation accelRot;
   
        accelRot = accel_.readPitchRoll();
        // std::cout << "[" << accelRot.roll << "," << accelRot.pitch << "]" << std::endl; 
        //END ACCEL

        //GYRO
        
        clock_t cT = clock();

        GyroDPS gDPS;
        gDPS = gyro_.readGyroDPS();
         
        unsigned long dT = cT - pT;
        pT = cT;
         
        double roll_c = gDPS.x*((double)dT/CLOCKS_PER_SEC);
        double pitch_c =  gDPS.y*((double)dT/CLOCKS_PER_SEC);
        double yaw_c = gDPS.z*((double)dT/CLOCKS_PER_SEC);


         
        // simple integrator
        xi = xi + roll_c;//gDPS.x*0.03;//((double)dT/CLOCKS_PER_SEC);
        yi = yi + pitch_c;//gDPS.y*0.03;//((double)dT/CLOCKS_PER_SEC); 
        zi = zi + yaw_c;//gDPS.z*0.03;//((double)dT/CLOCKS_PER_SEC);
        
        /*
        //runge-kutta integration 
        xi = xi + (1/6)*(roll[2]+(2*roll[1])+(2*roll[1])+roll_c);
        yi = yi + (1/6)*(pitch[2]+(2*pitch[1])+(2*pitch[1])+pitch_c); 
        zi = zi + (1/6)*(yaw[2]+(2*yaw[1])+(2*yaw[1])+yaw_c);
        roll[2] = roll[1];
        roll[1] = roll[0];
        roll[0] = roll_c;

        pitch[2] = pitch[1];
        pitch[1] = pitch[0];
        pitch[0] = pitch_c;

        yaw[2] = yaw[1];
        yaw[1] = yaw[0];
        yaw[0] = yaw_c;
        */
        //std::cout << "[" << gDPS.x << "," << gDPS.y << ","<< gDPS.z << ","

      
        angleF_roll = weight_gyro_*(angleF_roll + roll_c) + weight_accelerometer_*accelRot.roll;
        angleF_pitch = weight_gyro_*(angleF_pitch - pitch_c) + weight_accelerometer_*accelRot.pitch;

        publish_imu_data(accelRot.roll, accelRot.pitch, angleF_roll, angleF_pitch);
        //END GYRO
      }
      
      void publish_imu_data(double acc_roll, double acc_pitch, double roll, double pitch)
      {
        std_msgs::Float64MultiArray waist_state_msg;
        std_msgs::Float64MultiArray acceleration_msg;

        waist_state_msg.data.resize(3);
        waist_state_msg.data[0] = d2r(roll);
        waist_state_msg.data[1] = d2r(pitch);
        waist_state_msg.data[2] = 0.0;

        acceleration_msg.data.resize(3);
        acceleration_msg.data[0] = d2r(acc_roll);
        acceleration_msg.data[1] = d2r(acc_pitch);
        acceleration_msg.data[2] = 0.0;
 
        std::cout << "[" <<  acceleration_msg.data[0] << "," << acceleration_msg.data[1] <<  "," << waist_state_msg.data[0]  << "," << waist_state_msg.data[1]  << "]" << std::endl;
        imu_state_pub_.publish(waist_state_msg);
        accelerometer_state_pub_.publish(acceleration_msg);
      }

      double d2r(double degrees)
      {
         return (degrees * PI ) / 180 ;
      }

      void sleepcp(int milliseconds) // cross-platform sleep function
      {
          clock_t time_end;
          time_end = clock() + milliseconds * CLOCKS_PER_SEC/1000;
          while (clock() < time_end)
          {
          }
      }

  private:
        /**
         * Copy Ctor.
         */
        IMUIntegration(const IMUIntegration &other);

        /**
         * Assignment operator
         */
        IMUIntegration &operator=(const IMUIntegration &other);

        L3G4200D gyro_;
        ADXL345 accel_;
        double xi,yi,zi;
        clock_t pT;
        std::vector<double> roll;
        std::vector<double> pitch;
        std::vector<double> yaw;

        double angleF_roll;
        double angleF_pitch;
        ZenoI2CInterface zeno_i2c_interface_;

         /**
         * ROS node handle.
         */
        ros::NodeHandle nh_;

        /**
         * Publishers.
         */
        ros::Publisher imu_state_pub_;
        ros::Publisher accelerometer_state_pub_;

        /**
         * Imu node cycle rate.
         */
        double cycle_rate_;

        double weight_gyro_;
        double gyro_calib_x_;
        double gyro_calib_y_;
        double gyro_calib_z_;

        double weight_accelerometer_;
        double accelerometer_calib_x_;
        double accelerometer_calib_y_;
        double accelerometer_calib_z_;
};

int main(int argc, char **argv)
{
  ros::init(argc, argv, "zeno_imu_interface_node");

  ros::NodeHandle nh_("~");

    ZenoI2CInterface zeno_i2c_interface;
    if(zeno_i2c_interface.initilizeI2CInterface()) {
      IMUIntegration imu_intgration(nh_, zeno_i2c_interface);
      imu_intgration.start();
      zeno_i2c_interface.closeI2CInterface();
    } else {
      ROS_ERROR("I2C interface could not be initialized.");
    }
    return 0;
}
