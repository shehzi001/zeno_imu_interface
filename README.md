# Important usage Instruction
This repository provide a robot imu interface.

# Pre-requisite installations:
  * Install ROS-Indigo
  * Boost 1.45.0
  * CMake 2.8.7
  * MPSSE library

# Clone MPSSE repository and follow the ReadMe for installation in docs.

    https://github.com/devttys0/libmpsse.git

# Clone this repository and follow further instruction to install.

    git clone git@bitbucket.org:shehzi001/zeno_imu_interface.git

# Installation

    roscd && catkin build

# Test installation

  roslaunch zeno_imu_interface imu_interface.launch

