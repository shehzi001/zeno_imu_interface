# - Try to find MPSSE
# Once done this will define
#
#  MPSSE_FOUND - system has MPSSE
#  MPSSE_INCLUDES - the MPSSE include directory
#  MPSSE_LIBRARY - Link these to use MPSSE
#
#   Copyright (c) 2016 Shehzad Ahmed <shehzi001@gmail.com>
#
#  Redistribution and use is allowed according to the terms of the New
#  BSD license.
#  For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#

if (MPSSE_LIBRARY AND MPSSE_INCLUDES)
  # in cache already
  set(MPSSE_FOUND TRUE)
else (MPSSE_LIBRARY AND MPSSE_INCLUDES)

  find_library(MPSSE_LIBRARY
    NAMES
      NAMES libmpsse mpsse  
    PATHS
      /usr/lib
      /usr/local/lib
      /opt/local/lib
      /sw/lib
  )

  FIND_PATH(MPSSE_INCLUDES mpsse.h
      /usr/local/include
      /usr/include
  )

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(MPSSE DEFAULT_MSG MPSSE_LIBRARY MPSSE_INCLUDES)

  # show the MPSSE_INCLUDES and MPSSE_LIBRARY variables only in the advanced view
  mark_as_advanced(MPSSE_INCLUDES MPSSE_LIBRARY)

endif (MPSSE_LIBRARY AND MPSSE_INCLUDES)

